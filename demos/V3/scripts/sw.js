self.addEventListener('install', function(event) {
  // Perform install steps
      console.log('ServiceWorker install successful : ', event);
});

self.addEventListener('fetch', function(event) {
      console.log('ServiceWorker fetch : ', event);
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        console.log('Cache hit - return response : ', event.request);
        // Cache hit - return response
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});